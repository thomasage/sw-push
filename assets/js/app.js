import '../css/app.scss'

import $ from 'jquery'

const KEY_PUBLIC = 'BAAL4RkzpZtzWCve0J7beRyq5cL9K1lB5RMcE_jvqSC4skpsey23_cKHnlNTk5ip9d8HjnnOR7TLnPgIRf2fQjA'

navigator.serviceWorker.register('/sw.js')
  .then(registration => console.log('SW registered. Scope is', registration.scope))
  .catch(error => console.warn('Unable to register SW:', error))

$(function () {
  $('#subscribe').on('click', subscribe)
})

const urlB64ToUint8Array = base64String => {
  const padding = '='.repeat((4 - (base64String.length % 4)) % 4)
  const base64 = (base64String + padding).replace(/-/g, '+').replace(/_/g, '/')
  const rawData = atob(base64)
  const outputArray = new Uint8Array(rawData.length)
  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i)
  }
  return outputArray
}

function subscribe () {

  Notification.requestPermission()
    .then(result => 'granted' === result ? Promise.resolve() : Promise.reject(403))
    .then(() => {
      navigator.serviceWorker.ready
        .then(registration => registration.pushManager.subscribe({
          userVisibleOnly: true,
          applicationServerKey: urlB64ToUint8Array(KEY_PUBLIC)
        }))
        .then(subscription => {
          const options = {
            body: JSON.stringify(subscription),
            headers: { 'Content-Type': 'application/json' },
            method: 'POST'
          }
          fetch('/save.php', options)
            .then(response => console.log('Response:', response))
        })
    })

}
