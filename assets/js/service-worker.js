const version = 5

self.addEventListener('install', () => console.log(`SW v${version} installed`))

self.addEventListener('activate', () => console.log(`SW v${version} activated`))

self.addEventListener('push', event => {
  console.log('Event received', event)
  const payload = event.data.json()
  notify(payload.message)
})

function notify (message) {

  if ('granted' !== Notification.permission) {
    return
  }

  const options = {
    body: message,
    icon: '/favicon.ico'
  }
  self.registration.showNotification('Notification from SW', options)
    .then(() => console.log('Notification send'))
    .catch(() => console.warn('Notification failed'))

}

import { precacheAndRoute } from 'workbox-precaching'
import { registerRoute } from 'workbox-routing/registerRoute.mjs'
import { CacheFirst } from 'workbox-strategies'

precacheAndRoute(self.__WB_MANIFEST)

registerRoute(/\/index.html/, new CacheFirst)
