<?php
declare(strict_types=1);

namespace Deployer;

require 'recipe/common.php';

// Project name
set('application', 'sw-push');

set('keep_releases', 1);

// Project repository
set('repository', 'https://gitlab.com/thomasage/sw-push');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
set('shared_files', ['config.php', 'token.json']);
set('shared_dirs', []);

// Writable dirs by web server
set('writable_dirs', []);
set('allow_anonymous_stats', false);

// Hosts

inventory(__DIR__.'/hosts.yaml');

// Tasks

desc('Compile and upload asset');
task(
    'deploy:assets',
    static function (): void {
        runLocally('yarn build');
        upload('public/build', '{{release_path}}/public');
    }
);

desc('Deploy your project');
task(
    'deploy',
    [
        'deploy:info',
        'deploy:prepare',
        'deploy:lock',
        'deploy:release',
        'deploy:update_code',
        'deploy:shared',
        'deploy:writable',
        'deploy:vendors',
        'deploy:clear_paths',
        'deploy:symlink',
        'deploy:unlock',
        'cleanup',
        'success',
    ]
);

before('deploy:writable', 'deploy:assets');

// If deploy fails automatically unlock.

after('deploy:failed', 'deploy:unlock');

// [Optional] Specific to OVH

set('bin/php', '/usr/local/php7.3/bin/php');
