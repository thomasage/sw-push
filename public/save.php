<?php
declare(strict_types=1);

require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../config.php';

$filename = sprintf('%s/%s.json', DB_CLIENTS, md5(uniqid((string)mt_rand(), true)));

if (!file_exists(DB_CLIENTS) && !mkdir(DB_CLIENTS, 0700, true) && !is_dir(DB_CLIENTS)) {
    throw new \RuntimeException(sprintf('Directory "%s" was not created', DB_CLIENTS));
}

file_put_contents($filename, file_get_contents('php://input'));
