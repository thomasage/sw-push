<?php
declare(strict_types=1);

use Minishlink\WebPush\Subscription;
use Minishlink\WebPush\WebPush;

require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../config.php';

$clients = glob(DB_CLIENTS.'/*.json');

if (isset($_REQUEST['delete'])) {

    $filename = sprintf('%s/%s.json', DB_CLIENTS, $_REQUEST['delete']);
    if (file_exists($filename)) {
        unlink($filename);
    }

    header('location:/publish.php');
    exit();

}

if (isset($_POST['send'], $_POST['clients'], $_POST['message']) && count($_POST['clients']) > 0) {

    $auth = [
        'GCM' => KEY_PRIVATE,
        'VAPID' => [
            'privateKey' => KEY_PRIVATE,
            'publicKey' => KEY_PUBLIC,
            'subject' => VAPID_SUBJECT,
        ],
    ];

    $notifications = [];

    foreach ($_POST['clients'] as $client) {

        $filename = sprintf('%s/%s.json', DB_CLIENTS, $client);
        if (!file_exists($filename) || !is_readable($filename)) {
            continue;
        }
        $token = json_decode(file_get_contents($filename), true, 512, JSON_THROW_ON_ERROR);
        $notifications[] = Subscription::create($token);

    }

    $payload = ['message' => $_POST['message']];

    $webPush = new WebPush($auth);
    foreach ($notifications as $notification) {
        $webPush->sendNotification($notification, json_encode($payload, JSON_THROW_ON_ERROR));
    }

    $reports = [];

    foreach ($webPush->flush() as $report) {
        $endpoint = $report->getRequest()->getUri()->__toString();
        if ($report->isSuccess()) {
            $reports[$endpoint] = true;
        } else {
            $reports[$endpoint] = $report->getReason();
        }
    }

    header('location:/publish.php?reports='.http_build_query($reports));
    exit();

}
?>
<html lang="en">
<head>
    <title>Publish</title>
    <style>
        body {
            width: 600px;
            margin: 1em auto;
            font-family: sans-serif;
            font-size: 16px;
        }

        button {
            font-size: inherit;
        }

        form > div {
            margin-top: 2em;
        }

        form > div > label {
            display: block;
            margin-bottom: 0.5em;
        }

        textarea {
            width: 100%;
        }
    </style>
</head>
<body>
<form method="post">
    <div>
        <?php foreach ($clients as $c => $client): ?>
            <?php $name = basename($client, '.json'); ?>
            <div>
                <input type="checkbox"
                       name="clients[]"
                       value="<?php echo $name; ?>"
                       id="client<?php echo $c; ?>"/>
                <label for="client<?php echo $c; ?>">
                    <?php echo $name; ?>
                </label>
                <a href="?delete=<?php echo $name; ?>">delete</a>
            </div>
        <?php endforeach; ?>
    </div>
    <div>
        <label for="message">Message</label>
        <textarea name="message" rows="10" id="message" required></textarea>
    </div>
    <div>
        <button type="submit" name="send">Send message</button>
    </div>
</form>
</body>
</html>
